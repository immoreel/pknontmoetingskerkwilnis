# ReadMe
This repo contains the codebase for the website pknontmoetingskerkwilnis.nl. 

## Some handy commands:
Zip web directory excluding files dir:  
`zip -r web.zip web -x web/sites/default/files/*`

Export database  
`drush sql-dump --gzip --result-file=/Users/wouter/Docker/sites/pknontmoetingskerkwilnis/ontmoetingskerk_`date +"%Y-%m-%d"`.sql`