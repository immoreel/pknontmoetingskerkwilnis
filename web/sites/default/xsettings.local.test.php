<?php
/**
 * @file
 * Drupal site-specific configuration file.
 */

/**
 * Database settings.
 */
$databases = array(
  'default' => array(
    'default' => array(
      'database' => 'dev_ontmoetingsk_drupal',
      'username' => 'dev_pkn',
      'password' => '?E0cbr70',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);

/**
 * Access control for update.php script.
 */
$update_free_access = FALSE;

/**
 * Salt for one-time login links and cancel links, form tokens, etc.
 */
$drupal_hash_salt = '4yUdJ1KY2UmzzAdtjpxPTruJIGBexJpKq1xqIHJIfXM';

/**
 * Base URL (optional).
 */
# $base_url = 'http://www.example.com';  // NO trailing slash!

/**
 * This is needed to be logged in on multiple domains.
 */
# $cookie_domain = '';

/**
 * PHP settings.
 */
ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);
# ini_set('pcre.backtrack_limit', 200000);
# ini_set('pcre.recursion_limit', 200000);

/**
 * Force HTTPS! (a bit nasty but some servers don't set this?).
 */
# $_SERVER['HTTPS'] = 'on';

/**
 * Google tag manager code per environment.
 */
# $conf['google_tag_container_id'] = '';

/**
 * Variable overrides.
 */
# $conf['site_name'] = 'My Drupal site';
# $conf['theme_default'] = 'garland';
# $conf['anonymous'] = 'Visitor';
$conf['finalist_tripolis_register_api_client'] = '';
$conf['finalist_tripolis_register_api_username'] = '';
$conf['finalist_tripolis_register_api_password'] = '';
$conf['finalist_tripolis_register_api_db'] = '';

/**
 * Fast 404 pages.
 */
$conf['404_fast_paths_exclude'] = '/\/(?:styles)\//';
$conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$conf['404_fast_html'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';
# drupal_fast_404();
