<?php

/**
 * @file
 * pkn_ontmoetingskerk_wilnis_content_type_page.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function pkn_ontmoetingskerk_wilnis_content_type_page_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_iframe'.
  $field_bases['field_iframe'] = array(
    'active' => 1,
    'cardinality' => 3,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_iframe',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'iframe',
    'settings' => array(
      'class' => 'field-iframe',
      'frameborder' => 0,
      'scrolling' => 'no',
      'tokensupport' => 2,
      'transparency' => 1,
    ),
    'translatable' => 0,
    'type' => 'iframe',
  );

  return $field_bases;
}
