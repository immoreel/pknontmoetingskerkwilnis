<?php

/**
 * @file
 * pkn_ontmoetingskerk_wilnis_content_type_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pkn_ontmoetingskerk_wilnis_content_type_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
