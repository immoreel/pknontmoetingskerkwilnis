<?php

/**
 * @file
 * pkn_ontmoetingskerk_wilnis_content_type_headerimages.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function pkn_ontmoetingskerk_wilnis_content_type_headerimages_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-headerimages-field_image'.
  $field_instances['node-headerimages-field_image'] = array(
    'bundle' => 'headerimages',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_image',
    'label' => 'Afbeelding',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'epsacrop' => array(
        'styles' => array(
          'headers' => 'headers',
        ),
      ),
      'file_directory' => 'headers',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '1218x330',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 32,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Afbeelding');

  return $field_instances;
}
