<?php

/**
 * @file
 * pkn_ontmoetingskerk_wilnis_content_type_headerimages.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pkn_ontmoetingskerk_wilnis_content_type_headerimages_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_headerimages:node/add/headerimages.
  $menu_links['navigation_headerimages:node/add/headerimages'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/headerimages',
    'router_path' => 'node/add/headerimages',
    'link_title' => 'Headerimages',
    'options' => array(
      'attributes' => array(
        'title' => 'De afbeeldingen die aan de bovenkant van de site getoond worden, plaats hier sfeerfoto\'s',
      ),
      'identifier' => 'navigation_headerimages:node/add/headerimages',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_inhoud-toevoegen:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Headerimages');

  return $menu_links;
}
