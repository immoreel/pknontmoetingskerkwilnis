<?php

/**
 * @file
 * pkn_ontmoetingskerk_wilnis_content_type_headerimages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pkn_ontmoetingskerk_wilnis_content_type_headerimages_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pkn_ontmoetingskerk_wilnis_content_type_headerimages_node_info() {
  $items = array(
    'headerimages' => array(
      'name' => t('Headerimages'),
      'base' => 'node_content',
      'description' => t('De afbeeldingen die aan de bovenkant van de site getoond worden, plaats hier sfeerfoto\'s'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
