<?php

/**
 * @file
 * pkn_ontmoetingskerk_wilnis_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pkn_ontmoetingskerk_wilnis_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
