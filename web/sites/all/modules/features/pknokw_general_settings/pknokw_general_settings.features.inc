<?php

/**
 * @file
 * pknokw_general_settings.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pknokw_general_settings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "linkit" && $api == "linkit_profiles") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_fontyourface_features_default_font().
 */
function pknokw_general_settings_fontyourface_features_default_font() {
  return array(
    'Quicksand 300 (latin)' => array(
      'name' => 'Quicksand 300 (latin)',
      'enabled' => 1,
      'url' => 'http://www.google.com/webfonts/family?family=Quicksand&subset=latin#300',
      'provider' => 'google_fonts_api',
      'css_selector' => 'h1, ul.menu, .streamer h2.block-title, .block-bible th ',
      'css_family' => 'Quicksand',
      'css_style' => 'normal',
      'css_weight' => 300,
      'css_fallbacks' => '',
      'foundry' => '',
      'foundry_url' => '',
      'license' => '',
      'license_url' => '',
      'designer' => '',
      'designer_url' => '',
      'metadata' => 'a:2:{s:4:"path";s:13:"Quicksand:300";s:6:"subset";s:5:"latin";}',
    ),
  );
}
