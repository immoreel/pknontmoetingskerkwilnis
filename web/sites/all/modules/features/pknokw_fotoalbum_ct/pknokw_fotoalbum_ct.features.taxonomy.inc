<?php

/**
 * @file
 * pknokw_fotoalbum_ct.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function pknokw_fotoalbum_ct_taxonomy_default_vocabularies() {
  return array(
    'taakgroepen' => array(
      'name' => 'Taakgroepen',
      'machine_name' => 'taakgroepen',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
