<?php

/**
 * @file
 * pknokw_fotoalbum_ct.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pknokw_fotoalbum_ct_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_fotoalbums:node/add/fotoalbums.
  $menu_links['navigation_fotoalbums:node/add/fotoalbums'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/fotoalbums',
    'router_path' => 'node/add/fotoalbums',
    'link_title' => 'Fotoalbums',
    'options' => array(
      'attributes' => array(
        'title' => 'Verzameling van foto\'s',
      ),
      'identifier' => 'navigation_fotoalbums:node/add/fotoalbums',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_inhoud-toevoegen:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Fotoalbums');

  return $menu_links;
}
