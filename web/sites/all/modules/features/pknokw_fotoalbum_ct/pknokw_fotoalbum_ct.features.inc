<?php

/**
 * @file
 * pknokw_fotoalbum_ct.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pknokw_fotoalbum_ct_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pknokw_fotoalbum_ct_node_info() {
  $items = array(
    'fotoalbums' => array(
      'name' => t('Fotoalbums'),
      'base' => 'node_content',
      'description' => t('Verzameling van foto\'s'),
      'has_title' => '1',
      'title_label' => t('Titel'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
