<?php

/**
 * @file
 * pkn_ontmoetingskerk_wilnis_content_type_webform.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pkn_ontmoetingskerk_wilnis_content_type_webform_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pkn_ontmoetingskerk_wilnis_content_type_webform_node_info() {
  $items = array(
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Formulieren'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
