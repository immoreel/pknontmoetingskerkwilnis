<?php

/**
 * @file
 * pkn_ontmoetingskerk_wilnis_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function pkn_ontmoetingskerk_wilnis_permissions_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  // Exported role: editor 21-40.
  $roles['editor 21-40'] = array(
    'name' => 'editor 21-40',
    'weight' => 7,
  );

  // Exported role: editor 41-64.
  $roles['editor 41-64'] = array(
    'name' => 'editor 41-64',
    'weight' => 6,
  );

  // Exported role: editor 65+.
  $roles['editor 65+'] = array(
    'name' => 'editor 65+',
    'weight' => 5,
  );

  // Exported role: editor beroepingswerk.
  $roles['editor beroepingswerk'] = array(
    'name' => 'editor beroepingswerk',
    'weight' => 14,
  );

  // Exported role: editor diakonie.
  $roles['editor diakonie'] = array(
    'name' => 'editor diakonie',
    'weight' => 12,
  );

  // Exported role: editor eredienst.
  $roles['editor eredienst'] = array(
    'name' => 'editor eredienst',
    'weight' => 9,
  );

  // Exported role: editor gemeente.
  $roles['editor gemeente'] = array(
    'name' => 'editor gemeente',
    'weight' => 15,
  );

  // Exported role: editor jeugd.
  $roles['editor jeugd'] = array(
    'name' => 'editor jeugd',
    'weight' => 4,
  );

  // Exported role: editor publiciteit.
  $roles['editor publiciteit'] = array(
    'name' => 'editor publiciteit',
    'weight' => 10,
  );

  // Exported role: editor rentmeesters.
  $roles['editor rentmeesters'] = array(
    'name' => 'editor rentmeesters',
    'weight' => 11,
  );

  // Exported role: editor voto.
  $roles['editor voto'] = array(
    'name' => 'editor voto',
    'weight' => 8,
  );

  // Exported role: editor zwo.
  $roles['editor zwo'] = array(
    'name' => 'editor zwo',
    'weight' => 13,
  );

  // Exported role: webmaster.
  $roles['webmaster'] = array(
    'name' => 'webmaster',
    'weight' => 3,
  );

  return $roles;
}
