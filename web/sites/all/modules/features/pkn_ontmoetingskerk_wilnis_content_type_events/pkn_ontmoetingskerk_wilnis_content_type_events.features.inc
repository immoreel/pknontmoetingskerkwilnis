<?php

/**
 * @file
 * pkn_ontmoetingskerk_wilnis_content_type_events.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pkn_ontmoetingskerk_wilnis_content_type_events_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pkn_ontmoetingskerk_wilnis_content_type_events_node_info() {
  $items = array(
    'event' => array(
      'name' => t('Gebeurtenissen'),
      'base' => 'node_content',
      'description' => t('Dit zijn items waar een datum aan gekoppeld is. Bijvoorbeeld een jeugddienst, kamp, een ouderenmiddag of iets dergelijks'),
      'has_title' => '1',
      'title_label' => t('Titel'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
