<?php

/**
 * @file
 * pkn_ontmoetingskerk_wilnis_blocks.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pkn_ontmoetingskerk_wilnis_blocks_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['menu_block-1'] = array(
    'cache' => -1,
    'css_class' => 'submenu',
    'custom' => 0,
    'delta' => 1,
    'module' => 'menu_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'ontmoetingskerk' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'ontmoetingskerk',
        'weight' => 0,
      ),
      'ontmoetingskerk42' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'ontmoetingskerk42',
        'weight' => -17,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['menu_block-2'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 2,
    'module' => 'menu_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'ontmoetingskerk' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'ontmoetingskerk',
        'weight' => 0,
      ),
      'ontmoetingskerk42' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'ontmoetingskerk42',
        'weight' => -18,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['pknokw_blocks-foodbank'] = array(
    'cache' => 8,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'foodbank',
    'module' => 'pknokw_blocks',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'ontmoetingskerk' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'ontmoetingskerk',
        'weight' => 0,
      ),
      'ontmoetingskerk42' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'ontmoetingskerk42',
        'weight' => -15,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => -12,
      ),
      'bartik' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'ontmoetingskerk' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'ontmoetingskerk',
        'weight' => -12,
      ),
      'ontmoetingskerk42' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'ontmoetingskerk42',
        'weight' => -16,
      ),
      'seven' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => -10,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'ontmoetingskerk' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'ontmoetingskerk',
        'weight' => -10,
      ),
      'ontmoetingskerk42' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'ontmoetingskerk42',
        'weight' => -17,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
