<?php

/**
 * @file
 * pkn_ontmoetingskerk_wilnis_paragraphs.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function pkn_ontmoetingskerk_wilnis_paragraphs_image_default_styles() {
  $styles = array();

  // Exported image style: full_width.
  $styles['full_width'] = array(
    'label' => 'full width',
    'effects' => array(
      6 => array(
        'name' => 'epsacrop_crop',
        'data' => array(
          'width' => 746,
          'height' => '',
          'anchor' => 'center-center',
          'disable_scale' => 1,
          'jcrop_settings' => array(
            'aspect_ratio' => '',
            'bgcolor' => '#f0f3f7',
            'bgopacity' => 0.6,
            'fallback' => 0,
          ),
        ),
        'weight' => -10,
      ),
      5 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 746,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => -9,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_paragraphs_info().
 */
function pkn_ontmoetingskerk_wilnis_paragraphs_paragraphs_info() {
  $items = array(
    '2_images' => array(
      'name' => '2 afbeeldingen',
      'bundle' => '2_images',
      'locked' => '1',
    ),
    '3_images' => array(
      'name' => '3 Afbeeldingen',
      'bundle' => '3_images',
      'locked' => '1',
    ),
    'afbeelding_plus_tekst' => array(
      'name' => 'Afbeelding plus tekst',
      'bundle' => 'afbeelding_plus_tekst',
      'locked' => '1',
    ),
    'full_image' => array(
      'name' => '1 afbeelding (hele breedte)',
      'bundle' => 'full_image',
      'locked' => '1',
    ),
    'iframe' => array(
      'name' => 'iframe',
      'bundle' => 'iframe',
      'locked' => '1',
    ),
    'tekst' => array(
      'name' => 'Tekst',
      'bundle' => 'tekst',
      'locked' => '1',
    ),
  );
  return $items;
}
