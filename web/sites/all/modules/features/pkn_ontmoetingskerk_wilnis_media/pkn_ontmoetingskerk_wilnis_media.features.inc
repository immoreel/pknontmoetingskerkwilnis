<?php

/**
 * @file
 * pkn_ontmoetingskerk_wilnis_media.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pkn_ontmoetingskerk_wilnis_media_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_type") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function pkn_ontmoetingskerk_wilnis_media_image_default_styles() {
  $styles = array();

  // Exported image style: album_thumbs.
  $styles['album_thumbs'] = array(
    'label' => 'album_thumbs',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 160,
          'height' => 160,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: extra_large.
  $styles['extra_large'] = array(
    'label' => 'Extra large',
    'effects' => array(
      9 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1218,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: headers.
  $styles['headers'] = array(
    'label' => 'Headers',
    'effects' => array(
      8 => array(
        'name' => 'epsacrop_crop',
        'data' => array(
          'width' => 1218,
          'height' => 330,
          'anchor' => 'center-center',
          'disable_scale' => 0,
          'jcrop_settings' => array(
            'aspect_ratio' => '',
            'bgcolor' => 'black',
            'bgopacity' => 0.6,
            'fallback' => 0,
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: homepage.
  $styles['homepage'] = array(
    'label' => 'homepage',
    'effects' => array(
      6 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 710,
          'height' => 473,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: larger.
  $styles['larger'] = array(
    'label' => 'groter',
    'effects' => array(
      13 => array(
        'name' => 'epsacrop_crop',
        'data' => array(
          'width' => 490,
          'height' => 315,
          'anchor' => 'center-center',
          'disable_scale' => 0,
          'jcrop_settings' => array(
            'aspect_ratio' => '',
            'bgcolor' => 'black',
            'bgopacity' => 0.6,
            'fallback' => 0,
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: medium_crop.
  $styles['medium_crop'] = array(
    'label' => 'Medium crop',
    'effects' => array(
      3 => array(
        'name' => 'epsacrop_crop',
        'data' => array(
          'width' => 234,
          'height' => 315,
          'anchor' => 'center-center',
          'disable_scale' => 0,
          'jcrop_settings' => array(
            'aspect_ratio' => '',
            'bgcolor' => '#f0f3f7',
            'bgopacity' => 0.6,
            'fallback' => 0,
          ),
        ),
        'weight' => 1,
      ),
      4 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 315,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: promo.
  $styles['promo'] = array(
    'label' => 'promo',
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 220,
          'height' => 480,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: sidebar_image.
  $styles['sidebar_image'] = array(
    'label' => 'sidebar image',
    'effects' => array(
      8 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 282,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: square_thumbnail.
  $styles['square_thumbnail'] = array(
    'label' => 'square_thumbnail',
    'effects' => array(
      7 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 100,
          'height' => 100,
          'weight' => 0,
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}
