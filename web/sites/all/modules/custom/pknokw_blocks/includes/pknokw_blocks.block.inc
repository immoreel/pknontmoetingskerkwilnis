<?php

/**
 * @file
 * Foodbank block.
 */

function _pknokw_blocks_foodbank_content() {
  // Get title.
  $title = variable_get('pknokw_blocks_foodbank_title');

  //Get the text.
  $text = variable_get('pknokw_blocks_foodbank_content');
  $content = check_markup($text['value'], $text['format']);

  // Get the image.
  $image = variable_get('pknokw_blocks_foodbank_image');
  $image_file = file_load($image);
  $image_style = 'sidebar_image';
  $rendered_image = '';
  if (isset($image_file->uri)) {
    $rendered_image = theme(
      'image_style',
      [
        'path' => $image_file->uri,
        'alt' => '',
        'style_name' => $image_style,
        'attributes' => ['class' => 'sources-overview'],
      ]
    );
  }

  return theme('pknokw_foodbank', [
    'title' => $title,
    'content' => $content,
    'image' => $rendered_image,
  ]);

}
