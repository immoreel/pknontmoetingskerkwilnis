<?php

/**
 * @file
 * Admin functionality.
 */

function pknokw_blocks_foodbank_admin(array $form, array &$form_state) {
  $form['pknokw_blocks_foodbank_title'] = [
    '#type' => 'textfield',
    '#title' => t('title'),
    '#description' => t('The title of the block.'),
    '#default_value' => variable_get('pknokw_blocks_foodbank_title'),
    '#weight' => 0,
  ];

  $text = variable_get('pknokw_blocks_foodbank_content');
  $form['pknokw_blocks_foodbank_content'] = [
    '#type' => 'text_format',
    '#title' => t('Text.'),
    '#default_value' => $text['value'],
    '#format' => $text['format'],
    '#weight' => 1,
  ];

  $form['pknokw_blocks_foodbank_image'] = [
    '#title' => t("image"),
    '#type' => 'managed_file',
    '#description' => t('Allowed file extensions: png, gif, jpg, jpeg.'),
    '#default_value' => variable_get('pknokw_blocks_foodbank_image'),
    '#upload_validators' => [
      'file_validate_extensions' => ['png gif jpg jpeg'],
    ],
    '#upload_location' => 'public://pknokw_blocks/',
    '#weight' => 2,
  ];

  //return system_settings_form($form);

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function pknokw_blocks_foodbank_admin_submit(array $form, array &$form_state) {
  $values = $form_state['values'];

  // Loop through $values, setting variables.
  foreach ($values as $variable_name => $variable_value) {
    if (strstr($variable_name, 'pknokw_blocks_foodbank')) {
      variable_set($variable_name, $variable_value);
    }
  }

  // Loop through values, checking for files.
  foreach ($values as $key => $value) {
    if (strstr($key, 'image') && !empty($value)) {
      $file = file_load($value);
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      file_usage_add($file, 'pknokw_blocks', 'page', $value);
    }
  }
}
