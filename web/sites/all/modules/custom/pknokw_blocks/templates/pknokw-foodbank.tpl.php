<?php

/**
 * @file
 * Template for the foodbank block.
 */
?>

<div class="foodbank">
  <?php print $variables['image'] ?>

  <h2><?php print $variables['title'] ?></h2>
  <?php print $variables['content'] ?>
</div>
