<?php

/**
 * @file
 * Default drush aliases.drushrc.php file.
 */

/**
 * These are the default configuration so that
 * everyone can just overwrite the different settings.
 */


$aliases["dev"] = array (
  'root' => '/var/www/html/web',
  'uri' => 'http://rekenkamer.test',
  'path-aliases' => array(
    '%dump' => '/tmp/rekenkamer.sql',
    '%files' => 'files',
  ),
);

$aliases['test'] = array(
  'root' => '/var/www/ark-intranet/web',
  'remote-host' => '192.168.2.50',
  'remote-user' => 'root',
  'uri' => 'https://t-ark-intranet.finalist.nl/',
  'path-aliases' => array(
    '%dump' => '/tmp/t_rekenkamer.sql',
    '%files' => 'files',
   ),
);
